﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Security.Cryptography;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Text;
using System.IO;

using Amazon;
using Amazon.EC2;
using Amazon.EC2.Model;
using Amazon.SimpleDB;
using Amazon.SimpleDB.Model;
using Amazon.S3;
using Amazon.S3.Model;

namespace ProcessorgBackEnd
{
    public partial class _Default : System.Web.UI.Page
    {
        protected IAmazonEC2 ec2;
        protected IAmazonS3 s3;
        protected IAmazonSimpleDB sdb;

        static string NewPass = "";
        static int ID2UserFromDb = 0;
        static int NewIdCompany = 0;
        static string Company_ID = "";
        static string People_ID = "";
        static string People_pass = "";

        static bool errors = false;

        static string CS = ConfigurationManager.ConnectionStrings["Prs_Connect"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            //using (SqlConnection con = new SqlConnection(CS))
            //{
            //    con.Open();
            //    DataSet ds1 = new DataSet();
            //    SqlDataAdapter adapter1 = new SqlDataAdapter
            //    {
            //        SelectCommand = new SqlCommand(
            //            " USE [Processorg1SQL] SELECT * FROM People WHERE ID2=@ID2",
            //            con)
            //    };
            //    adapter1.SelectCommand.Parameters.AddWithValue("@ID2", 70);
            //    adapter1.Fill(ds1);
            //    DataRow row1 = ds1.Tables[0].Rows[0];
            //    DataSet ds2 = new DataSet();
            //    SqlConnection con2 =
            //        new SqlConnection(
            //            "Data Source=third3.ch8lyrdvaans.eu-west-1.rds.amazonaws.com,1433;Initial Catalog=677901Urn8i;User ID=razoredge;Password=Test14");
            //    SqlDataAdapter adapter2 =
            //        new SqlDataAdapter(
            //            " SELECT * FROM People ",
            //            con2);
            //    adapter2.Fill(ds2);
            //    DataTable tabl2 = ds2.Tables[0];
            //    SqlCommandBuilder builder2 = new SqlCommandBuilder(adapter2);
            //    adapter2.UpdateCommand = builder2.GetUpdateCommand();
            //    DataRow row2 = tabl2.NewRow();
            //    row2.ItemArray = row1.ItemArray;
            //    tabl2.Rows.Add(row2);
            //    adapter2.Update(tabl2);
                
            //}
            //string hashorig = String.Format("{0}{1}{2}", 10, 10, "keqqwerty");
            //var hash22 = hashorig.GetHashCode();
            //string responceStr = String.Format(@"http://default-environment-sgim2pevak.elasticbeanstalk.com?Company_ID={0}&People_ID={1}&task={2}&hash={3}", 10, 10, "newCompanyAdmin", hash22);

           // this.ec2Placeholder.Text = "getInfoOf";
            string task = Request.QueryString["task"];
            if (task == null || task.Length <= 0) return;
            Company_ID = Request.QueryString["Company_ID"];
            People_ID = Request.QueryString["People_ID"];
            People_pass = Request.QueryString["People_param"];
            string hashFromUrl = Request.QueryString["hash"];
            string hash = String.Format("{0}{1}{2}{3}", Company_ID, People_ID, People_pass, "keqqwerty");
            string hash1 = ComputeStringMD5Hash(hash);
            if (hashFromUrl == hash1)
            {
                Console.WriteLine("good!");
            }
            else return;
            switch (task)
            {
                case "newCompanyAdmin":
                    AddNewRel(); // Добавляем связь пользователь-компания
                    getNewPass(); // формируем пароль пользователя
                    getNewIdCompany(); // получаем ИД_Компании исходя из имеющихся в базе
                    setPassToPeople(People_ID); // добавляем пароль юзеру
                    setIdToCompany(NewIdCompany); // обновляем таблицу компаний - присваиваем ИД_Компании
                    sendMailToAdmin(NewPass, NewIdCompany); // письмо с паролем админу на первый вход
                    break;
                case "login":
                    int logined = checkLogin();
                    bool UserIsPresent = CheckUser();
                    if (logined > 0)
                    {
                        startLogin();
                    }
                    else
                    {
                        startCreateNewDb(Company_ID);
                        startLogin();
                    }
                    break;
            }
        }

        private void startLogin()
        {
            bool UserIsPresent = CheckUser(); // Проверка логина и пароля
            if (UserIsPresent) // Логин успешен
            {
                int CompanyID = getCompanyID(); // Получение ид Компании
                if (CompanyID > 0) // Компания есть в таблице компаний
                {
                    var dbName = getDbName(CompanyID); // Получение имени базы
                    Response.Write(dbName); // Вывод имени  базы
                }
            }
        }

        private bool CheckUser()
        {
            bool userIsPresent = false;
            var passHash = ComputeStringMD5Hash(People_pass);
            var passHash1 = passHash.Substring(6, 6);
            using (SqlConnection con = new SqlConnection(CS))
            {
                con.Open();
                SqlCommand cmd =
                    new SqlCommand(
                        " USE [Processorg1SQL] " +
                        " SELECT ID2 FROM People WHERE Password=@Pass AND Work_email=@user",
                        con);
                cmd.Parameters.AddWithValue("@Pass", passHash1);
                cmd.Parameters.AddWithValue("@user", People_ID);
                ID2UserFromDb = Convert.ToInt32(cmd.ExecuteScalar());
                if (ID2UserFromDb > 0)
                {
                    userIsPresent = true;
                }
            }
            return userIsPresent;
        }

        private object getDbName(int CompanyID)
        {
            string DbName = String.Empty;
            string DbHash = String.Empty;
            using (SqlConnection con = new SqlConnection(CS))
            {
                con.Open();
                SqlCommand cmd =
                    new SqlCommand(
                        " USE [Processorg1SQL] " +
                        " SELECT DbName, DbHash  FROM Company_Peoples WHERE Company_ID = @company AND People_ID=@user",
                        con);
                cmd.Parameters.AddWithValue("@company", CompanyID);
                cmd.Parameters.AddWithValue("@user", ID2UserFromDb);
                var reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                       // Console.WriteLine("{0}\t{1}", reader.GetString(0),
                        //    reader.GetString(1));
                        DbName = reader.GetString(0);
                        DbHash = reader.GetString(1);
                    }
                }
                else
                {
                    Console.WriteLine("No rows found.");
                }
                reader.Close();
            }

            return DbName.Trim() + DbHash.Trim();
        }

        private int getCompanyID()
        {
            int ID3;
            using (SqlConnection con = new SqlConnection(CS))
            {
                con.Open();
                SqlCommand cmd =
                    new SqlCommand(
                        " USE [Processorg1SQL] " +
                        " SELECT ID3 FROM Company WHERE Company_ID = @company",
                        con);
                cmd.Parameters.AddWithValue("@company", Company_ID);
                ID3 = Convert.ToInt32(cmd.ExecuteScalar());
            }
            return ID3;
        }
     
        public string ComputeStringMD5Hash(string instr)
        {
            string strHash = string.Empty;

            foreach (byte b in new MD5CryptoServiceProvider().ComputeHash(Encoding.Default.GetBytes(instr)))
            {
                strHash += b.ToString("X2");
            }
            return strHash;
        }

        private int checkLogin()
        {
            var passHash = ComputeStringMD5Hash(People_pass);
            var passHash1 = passHash.Substring(6, 6);
            using (SqlConnection con = new SqlConnection(CS))
            {
                con.Open();
                SqlCommand cmd =
                    new SqlCommand(
                        " USE [Processorg1SQL] " +
                        " SELECT cp.Visited FROM Company_Peoples as cp " +
                        " LEFT JOIN People as p ON cp.People_ID=p.ID2 " +
                        " WHERE cp.Company_ID = (SELECT ID3 FROM Company WHERE Company_ID = @company) AND p.Password=@Pass AND p.Work_email=@user ",
                        con);
                cmd.Parameters.AddWithValue("@Pass", passHash1);
                cmd.Parameters.AddWithValue("@user", People_ID);
                cmd.Parameters.AddWithValue("@company", Company_ID);
                int visited = Convert.ToInt32(cmd.ExecuteScalar());
                    return visited;
            }
        }

        private void startCreateNewDb(string company)
        {
            string dbHash = GetPass(5);
            string dbName = company + dbHash;
            var passHash = ComputeStringMD5Hash(People_pass);
            var passHash1 = passHash.Substring(6, 6);
            CS = ConfigurationManager.ConnectionStrings["Serv_Connect"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(CS))
                {
                    con.Open();
                    string SqlQuery = @" CREATE DATABASE [" + dbName + "]; ";
                    SqlCommand comm = new SqlCommand(SqlQuery, con);
                    // Создаем новую базу данных с заданным именем
                    comm.ExecuteNonQuery();

                    string sqlquery = getStringCreateTables(dbName);
                    SqlCommand comm1 = new SqlCommand(sqlquery, con);
                    // Заполняем созданную базу данных структурой таблиц
                    comm1.ExecuteNonQuery();

                    SqlCommand comm2 =
                        new SqlCommand(
                            " USE [Processorg1SQL] " +
                            " SELECT id FROM Company_Peoples as cp " +
                            " LEFT JOIN People as p ON cp.People_ID=p.ID2 " +
                            " WHERE cp.Company_ID = (SELECT ID3 FROM Company WHERE Company_ID = @company) AND p.Password=@Pass AND p.Work_email=@user ",
                            con);
                    comm2.Parameters.AddWithValue("@Pass", passHash1);
                    comm2.Parameters.AddWithValue("@user", People_ID);
                    comm2.Parameters.AddWithValue("@company", Company_ID);
                    // Получаем ид строки для обновления данных в таблице связей.
                    int idRelativ = Convert.ToInt32(comm2.ExecuteScalar());

                    SqlCommand comm3 =
                        new SqlCommand(
                            " UPDATE Company_Peoples SET Visited=1, DbName=@DbName, DbHash=@DbHash WHERE id=@rel", con);
                    comm3.Parameters.AddWithValue("@rel", idRelativ);
                    comm3.Parameters.AddWithValue("@DbName", company);
                    comm3.Parameters.AddWithValue("@DbHash", dbHash);
                    // Обновление данных в таблице связей, указание хеша и названия базы.
                    // Указание, что вход выполнен, база создана.
                    comm3.ExecuteNonQuery();

                   
                }
                    // Создаем пользователя - админа в новой базе в таблице пользователей
                using (SqlConnection con = new SqlConnection(CS))
                {
                    con.Open();
                    DataSet ds1 = new DataSet();
                    SqlDataAdapter adapter1 = new SqlDataAdapter
                    {
                        SelectCommand = new SqlCommand(
                            " USE [Processorg1SQL] SELECT * FROM People WHERE ID2=@ID2",
                            con)
                    };
                    adapter1.SelectCommand.Parameters.AddWithValue("@ID2", ID2UserFromDb);
                    adapter1.Fill(ds1);
                    DataRow row1 = ds1.Tables[0].Rows[0];
                    DataSet ds2 = new DataSet();
                    SqlConnection con2 =
                        new SqlConnection(
                           String.Format("Data Source=third3.ch8lyrdvaans.eu-west-1.rds.amazonaws.com,1433;Initial Catalog={0};User ID=razoredge;Password=Test14", dbName));
                    SqlDataAdapter adapter2 =
                        new SqlDataAdapter(
                            " SELECT * FROM People ",
                            con2);
                    adapter2.Fill(ds2);
                    DataTable tabl2 = ds2.Tables[0];
                    SqlCommandBuilder builder2 = new SqlCommandBuilder(adapter2);
                    adapter2.UpdateCommand = builder2.GetUpdateCommand();
                    DataRow row2 = tabl2.NewRow();
                    row2.ItemArray = row1.ItemArray;
                    tabl2.Rows.Add(row2);
                    adapter2.Update(tabl2);

                }
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp.Message);
            }
        }

        private string getStringCreateTables(string dbName)
        {
            string CreateQueryString = string.Empty;
            using (SqlConnection con = new SqlConnection(CS))
            {
                con.Open();
                SqlCommand comm2 =
                    new SqlCommand(
                        " USE [Processorg1SQL] " +
                        " SELECT NewDbString FROM ServiseTable",
                        con);
                CreateQueryString = " USE [" + dbName + "] " + comm2.ExecuteScalar().ToString();
            }

            string sqlquery = CreateQueryString;
            return sqlquery;
        }
        
        private void AddNewRel()
        {
            using (SqlConnection con = new SqlConnection(CS))
            {
                con.Open();
                SqlCommand cmd =
                    new SqlCommand(
                        " USE [Processorg1SQL] " +
                        "insert into Company_Peoples (Company_ID,People_ID, Admin, Visited) values (@Company_ID,@People_ID, 1, 0)",
                        con);
                cmd.Parameters.AddWithValue("@Company_ID", Company_ID);
                cmd.Parameters.AddWithValue("@People_ID", People_ID);
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        private void setIdToCompany(int NewIdCompany)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(CS))
                {
                    con.Open();
                    string SqlQuery = String.Format(" USE [Processorg1SQL] " + " UPDATE Company SET Company_ID='{0}' WHERE ID3='{1}'; ", NewIdCompany, Company_ID);
                    SqlCommand comm = new SqlCommand(SqlQuery, con);
                    comm.ExecuteNonQuery();
                }
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp.Message);
            }
        }

        private void setPassToPeople(string People_ID)
        {
            try
            {
                var passHash = ComputeStringMD5Hash(NewPass);
                var passHash1 = passHash.Substring(6, 6);
                using (SqlConnection con = new SqlConnection(CS))
                {
                    con.Open();
                    string SqlQuery = String.Format(" USE [Processorg1SQL] " + " UPDATE People SET Password='{0}' WHERE ID2='{1}'; ", passHash1, People_ID);
                    SqlCommand comm = new SqlCommand(SqlQuery, con);
                    comm.ExecuteNonQuery();
                }
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp.Message);
            }
            
        }

        private void sendMailToAdmin(string NewPass, int NewIdCompany)
        {
            const String from = "baykovskys@gmail.com";
            const String TO = "processorg@ukr.net";

            const String SUBJECT = "New Credentials To Processorg";
            String BODY = string.Format(@"Yours new password: {0}   CompanyID: {1}", NewPass, NewIdCompany);

            const String SMTP_USERNAME = "AKIAIOEVVO6BG2SOL32A";
            const String SMTP_PASSWORD = "AsLuNIwUDRWRW315MSCa88QLY16Ifx6RSBLvOZWk5p75";

            const String HOST = "email-smtp.us-west-2.amazonaws.com";
            const int PORT = 587;
            System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(HOST, PORT);
            client.Credentials = new System.Net.NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);
            client.EnableSsl = true;
            try
            {
                Console.WriteLine("Attempting to send an email through the Amazon SES SMTP interface...");
                client.Send(from, TO, SUBJECT, BODY);
                Console.WriteLine("Email sent!");
            }
            catch (Exception ex)
            {
                Console.WriteLine("The email was not sent.");
                Console.WriteLine("Error message: " + ex.Message);
            }
        }

        private void getNewIdCompany()
        {
            try
            {
                //string maxIdCompanyFromDb = string.Empty;
                //using (SqlConnection con = new SqlConnection(CS))
                //{
                //    con.Open();
                //    string SqlQuery = " SELECT MAX(Company_ID)  FROM Company; ";
                //    SqlCommand comm = new SqlCommand(SqlQuery, con);
                //    maxIdCompanyFromDb = comm.ExecuteScalar().ToString();
                //}
               int IntCompany_ID = Convert.ToInt32(Company_ID);
                // Идиотская формула получения ИдКомпании обусловлена тем, что ЧИСЛОВОЙ ид в 
                // базе компаний кто-то пишет  вполе nvarchar.
                // Имеем то что имеем)
                NewIdCompany = IntCompany_ID + 677868;
                
                //NewIdCompany = Convert.ToInt32(maxIdCompanyFromDb) + 1;
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp.Message);
            }
        }

        private void getNewPass()
        {
            NewPass = GetPass(6);
        }

        public static string GetPass(int x)
        {
            string pass = "";
            var r = new Random();
            while (pass.Length < x)
            {
                Char c = (char)r.Next(33, 125);
                if (Char.IsLetterOrDigit(c))
                    pass += c;
            }
            return pass;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string dbName = "newtestDb";
            CS = ConfigurationManager.ConnectionStrings["Serv_Connect"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(CS))
                {
                    con.Open();
                    string SqlQuery = @" CREATE DATABASE ["+dbName+"]; ";
                    SqlCommand comm = new SqlCommand(SqlQuery, con);
                    comm.ExecuteNonQuery();
                }
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp.Message);
            }

        }
    }
}